#include <unistd.h>
#include "utility.h"
#include "pipe.h"
#include "history.h"
// add your #includes here

int main()
{
    int argc;
    char s[MAX];
    char line[MAX];
    char *command;
    history = linkedList();

	// You will need code to open .ushrc and .ush_history here

    FILE *conf;
    FILE *hist;
    //check access to config file
    //if != -1 the file exists and can be opened
    if(access(CONFIG, F_OK)!=-1) {
        //we have access to the file
        //lets read in the settings
        conf = fopen(CONFIG,"r");
        //read while not EOF
        while(!feof(conf)) {
            //get line from config file
            fgets(line, MAX, conf);
            //extract the variable setting from the line
            //ie: HISTFILECOUNT=100
            //this regex matches on the integer following the equal sign
            //which is then assigned to argc
            sscanf(line, "%[^=]=%d", s, &argc);

            //assign to appropriate variable
            //dependant on the value of s which which is now
            //thanks to sscanf, the value preceeding the equal sign in the line
            if(strcmp(HISTCOUNT_KEY, s)==0)
                hcount = argc;
            else if(strcmp(HISTFILECOUNT_KEY, s)==0)
                hfcount = argc;

        }
        //close the config file
        fclose(conf);
    } else {
        //if the config file does not exist, or for some reason we can't access it
        //set some sane defaults
        hfcount = D_HISTFILECOUNT;
        hcount = D_HISTCOUNT;
    }
    //done reading config file
    //start reading in history file
    if(access(HISTFILE,F_OK)!=-1) {
        //histfile exists
        //open histfile
        hist = fopen(HISTFILE,"r+");
        //read contents of history file
        //parse into linked list
        parseHistoryFile(hist);
    } else {
        //histfile doesn't exist
        //open a new histfile for writing
        hist = fopen(HISTFILE,"w+");
        //set starting position for history to zero
        initHist(0);
    }
    //done parsing or creating history file and initializing list
    //moving on to main input & execute loop

	printf("?: ");
	fgets(s, MAX, stdin);
	strip(s);

	while(strcmp(s, "exit") != 0)
	{
        //trip whitespace off command
        command = strip(s);

		// You will need fork it and pipe it and other code here
        if(strlen(command) > 0) {
            //execute command
            execCmd(command);
            //add to history
            addHistory(command);
        }

		// you will probably need code to clean up stuff

        printf("?: ");
        fgets(s, MAX, stdin);
        strip(s);

	}// end while

    //http://linux.die.net/man/2/ftruncate
    ftruncate(fileno(hist), 0);
    //http://pubs.opengroup.org/onlinepubs/009695399/functions/fileno.html
    rewind(hist);
    flushHistory(hist);
    fclose(hist);
    clearList(history, cleanCommand);
    free(history);
    history = NULL;

    return 0;

}// end main
