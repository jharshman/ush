//
// Created by jharshman on 10/9/15.
//

/**
 *
 * implementation of utility.h
 *
 * Provide basic utility functions
 * --------------------------------------------------------
 * char *strip(char *s);
 * void clean(int argc, char **argv);
 * char ** makeargs(char *s, int * argc, const char *delim);
 *
 *
 * */

#include "utility.h"

char *strip(char *s) {
    int x = 0;
    size_t len;
    len = strlen(s);
    char *end;

    //strip whitespace and replace with
    //null terminating character
    while (s[x] != '\0' && x < len) {
        if (s[x] == '\r')       s[x] = '\0';
        else if (s[x] == '\n')  s[x] = '\0';
        x++;
    }
    //strip leading whitespace
    while (*s == ' ')
        s++;
    //strip any whitespace on the end of the command
    end = s + strlen(s) - 1;
    while (end > s && *end == ' ')
        end--;
    *(end + 1) = '\0';
    return s;
}

//free memory associated with the command
//walks the char** array and frees the memory
void clean(int argc, char **argv) {
    int i;
    for(i = 0; i < argc; i++) {
        free(argv[i]);
        argv[i] = NULL;
    }
    free(argv);
    argv = NULL;
}

//primary tokenizing function
//expanded from previous labs to tokenize on the delim parameter
char ** makeargs(char *s, int * argc, const char *delim) {
    int index = 0;
    char *tokens[MAX];
    char *saveptr;
    char *token;
    char *cpy;
    char **toReturn;

    //if the size of the command is less than one,
    //we are going to set the count to -1 and
    //return a nullptr
    size_t len = strlen(s);
    if(len<1) {
        *argc = -1;
        return NULL;
    }
    //make a copy of the passed in command before tokenizing
    //we do this because strtok_r modifies the original
    cpy = (char*)calloc(strlen(s)+1, sizeof(char));
    strcpy(cpy, s);
    //tokenize based on the delimiter
    token = strtok_r(cpy, delim, &saveptr);
    //walk through the rest of the cstring and tokenize
    //copying each token into temporary holder which is then
    //in turn assigned into the tokens array
    do {
        char *tok = calloc(strlen(token) + 1, sizeof(char));
        strcpy(tok, token);
        tokens[index++] = tok;
    } while ((token = strtok_r(NULL, delim, &saveptr)) != NULL);
    //set the reference to argc to the value of index
    //index represents the number of tokens the cstring has been tokenized into
    *argc = index;
    //give memory to the char** variable we will be returning to the
    //calling function
    toReturn = calloc(index + 1, sizeof(char*));
    //now we are going to walk the tokens array and assign each
    //value to an index in char** toReturn
    for (int i = 0; i < index; i++)
        toReturn[i] = tokens[i];
    //free memory associated with the copy we made of the original cstring
    //before tokenization
    free (cpy);
    //return the char**
    return toReturn;
}

