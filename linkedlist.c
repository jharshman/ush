//
// Created by jharshman on 10/15/15.
//

/**
 *
 *
 * implementation of linkedlist.h
 *
 * circular, doubly linked list w/ dummy head node
 * -------------------------------------------------
 *
 * void sort(LinkedList * myList, int (*compare)(const void *, const void *));
 * void removeItem(LinkedList * myList, Node * newNode, void (*removeData)(void *), int (*compare)(const void *, const void *));
 * void clearList(LinkedList * myList, void (*removeData)(void *));
 * void printLastItems(FILE *outFile, const LinkedList * myList, void (*printData)(FILE *out_file, void * a), int num);
 * void addAfter(LinkedList *myList, Node *after, Node *newNode);
 * void addLast(LinkedList * myList, Node * newNode);
 * void addFirst(LinkedList * myList, Node * newNode);
 * void printList(FILE *outFile, const LinkedList * myList, void (*convertData)(FILE *out_file, void *));
 *
 * */

#include "linkedlist.h"

//build the node
//simply allocate memory for the node
//assign passed in data to the node and return
Node *build_node(void *data) {
    Node *temp = calloc(1, sizeof(Node));
    temp->data = data;
    return temp;
}

//constructor for linked list
//initializes circular doubly linked list
//with dummy head node
LinkedList *linkedList() {
    LinkedList *returnList = calloc(1, sizeof(LinkedList));
    Node *dummy = calloc(1, sizeof(Node));
    dummy->next = dummy;
    dummy->prev = dummy;
    returnList->head = dummy;
    return returnList;
}

//wrapper function for the addAfter function,
//since the list is circular, all we need to do is call addAfter
void addLast(LinkedList * myList, Node * newNode) {
    addAfter(myList, myList->head->prev, newNode);
}

//wrapper function for addAfter function,
//since the list is circular, all we need to do is call addAfter
void addFirst(LinkedList * myList, Node * newNode) {
    addAfter(myList, myList->head, newNode);
}

//add function
//this function is never called directly
//it is called by either addLast or addFirst wrapper functions
//it simply just adds the new node to the list
void addAfter(LinkedList *myList, Node *after, Node *newNode) {
    newNode->prev = after;
    newNode->next = after->next;
    newNode->next->prev = newNode;
    after->next = newNode;
    myList->size++;
}

//removes an item from the list
//un-links and decrements the size
//frees memory
void removeItem(LinkedList * myList, Node * newNode, void (*removeData)(void *), int (*compare)(const void *, const void *)) {
    Node *currentNode = myList->head;
    size_t currentIndex = 0;
    while (currentIndex < myList->size) {
        currentNode = currentNode->next;
        if (!compare(currentNode, newNode)) {
            currentNode->prev->next = currentNode->next;
            currentNode->next->prev = currentNode->prev;
            removeData(currentNode->data);
            free(currentNode);
            myList->size--;
            break;
        }
        if (currentNode == myList->head)
            break;
        currentIndex++;
    }
    removeData(newNode->data);
    free(newNode);
}

//clear the list
//walk the entire list, removing nodes, freeing memory, and decrementing size
//finally free the last node
void clearList(LinkedList * myList, void (*removeData)(void *)) {
    Node *currentNode = myList->head;
    while (myList->size > 0) {
        currentNode = currentNode->next;
        removeData(currentNode->data);
        free(currentNode->prev);
        myList->size--;
    }
    free(currentNode);
}

//print the linked list
//walk the list, call print command function in commands.c
//here that function will be passed as a function pointer and called within the function
void printList(FILE *outFile, const LinkedList * myList, void (*printData)(FILE *out_file, void * a)) {
    Node *currentNode = myList->head->next;
    while (currentNode != myList->head) {
        printData(outFile, currentNode->data);
        currentNode = currentNode->next;
    }
}

//here we are going to print the last records in the list out to the file
//as with printList, we are going to pass a pointer to the print function in commands.c
void printLastItems(FILE *outFile, const LinkedList * myList, void (*printData)(FILE *out_file, void * a), int num) {
    Node *currentNode = myList->head->next;
    int index = 0;
    int startAtIndex = myList->size - num;
    if (startAtIndex < 0)
        startAtIndex = 0;
    while (currentNode != myList->head) {
        if (index >= startAtIndex) {
            printData(outFile, currentNode->data);
        }
        currentNode = currentNode->next;
        index++;
    }
}

//utilize qsort to sort the list
//because qsort doesn't natively work on a linked list
//we walk the list and populate an array to pass to qsort,
//the after qsort is called and the list is sorted,
//we walk the list again, this time populating it with the new sorted values
//sort of a hack as we aren't actually moving nodes around, linking, un-linking, etc...
//but it gets the job done
void sort(LinkedList * myList, int (*compare)(const void *, const void *)) {
    void **nodeData = calloc((size_t)myList->size, sizeof(void*));
    Node *currentNode = myList->head;
    size_t index = 0;
    while (index < myList->size) {
        currentNode = currentNode->next;
        nodeData[index] = currentNode->data;
        index++;
    }
    qsort(nodeData, (size_t)myList->size, sizeof(void*), compare);
    currentNode = myList->head;
    index = 0;
    while (index < myList->size) {
        currentNode = currentNode->next;
        currentNode->data = nodeData[index];
        index++;
    }
    free(nodeData);
}

