//
// Created by jharshman on 10/15/15.
//

#include "history.h"
#include "utility.h"

int history_start = -1;
int hcount = 100;
int hfcount = 1000;
LinkedList *history = NULL;

void addHistory(char *command) {
    Commands *last_command;
    int id;

    if (canAddHistory(command)) {
        if (command[0] == BANG && sscanf(command, "!%d", &id) == 1) {
            last_command = historyNumbered(id);
            command = last_command->command;
        }
        addLast(history, build_node(buildCommand(command)));
    }
}

int canAddHistory(char *command) {
    Commands *current_command = NULL;
    int id;

    if (history->size == 0)
        return 1;

    if (strcmp(command, BANGBANG) == 0)
        return 0;

    if (command[0] == '!' && sscanf(command, "!%d", &id) == 1) {
        current_command = historyNumbered(id);
        if (current_command == NULL)
            return 0;

        command = current_command->command;
    }

    Commands *lastCommand = (Commands*)history->head->prev->data;
    return !commands_are_equal(lastCommand->command, command);
}

void parseHistoryFile(FILE *histfile) {
    int index;
    char command[MAX], line[MAX];

    while (fgets(line, MAX, histfile) != NULL) {
        if (sscanf(line, "%5d %s", &index, command) != 2)
            continue;
        if (history_start == -1)
            history_start = index;

        addLast(history, build_node(buildCommand(command)));
    }

    initHist(0);
}

void flushHistory(FILE *histfile) {
    printLastItems(histfile, history, printCommand, hfcount);
}

void initHist(int start) {
    if (history_start != -1)
        return;
    history_start = start;
}

Commands *historyNumbered(int num) {
    Node *cur;
    Commands *cur_command;
    cur = history->head;
    while ((cur = cur->next) != history->head) {
        cur_command = (Commands*)cur->data;
        if (cur_command->num == num)
            return cur_command;
    }

    return NULL;
}