//
// Created by jharshman on 10/15/15.
//

#ifndef USH_PIPE_H
#define USH_PIPE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utility.h"

void execCmd(char *command);

#endif //USH_PIPE_H
