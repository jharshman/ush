//
// Created by jharshman on 10/15/15.
//

#ifndef USH_LINKEDLIST_H
#define USH_LINKEDLIST_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct node
{
    void * data;
    struct node * next;
    struct node * prev;
};
typedef struct node Node;


struct linkedlist
{
    Node * head;
    size_t size;
};
typedef struct linkedlist LinkedList;

LinkedList * linkedList();
Node * buildNode(int argc, const char ** argv, void *(*buildData)(int argc, const char ** argv) );
Node *build_node(void *data);

void sort(LinkedList * myList, int (*compare)(const void *, const void *));
void removeItem(LinkedList * myList, Node * newNode, void (*removeData)(void *), int (*compare)(const void *, const void *));
void clearList(LinkedList * myList, void (*removeData)(void *));
void printLastItems(FILE *outFile, const LinkedList * myList, void (*printData)(FILE *out_file, void * a), int num);
void addAfter(LinkedList *myList, Node *after, Node *newNode);
void addLast(LinkedList * myList, Node * newNode);
void addFirst(LinkedList * myList, Node * newNode);
void printList(FILE *outFile, const LinkedList * myList, void (*convertData)(FILE *out_file, void *));

#endif //USH_LINKEDLIST_H
