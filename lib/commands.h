//
// Created by jharshman on 10/15/15.
//

#ifndef USH_COMMANDS_H
#define USH_COMMANDS_H
#define BANG '!'
#define BANGBANG "!!"
#define SPACE " "

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef void (*executable_command)(int argc, char **argv);

typedef struct exe {
    const char *command;
    executable_command exec;
} executable;

struct commands
{
    char *command;
    int num;
};

typedef struct cmdPart {
    char **command;
    int count;
    int type;

} cmdPart;

typedef struct commands Commands;

void * buildCommand(char *s);
void *build_command_part(int argc, char **argv, int type);
int commands_are_equal(char *com_a, char *com_b);
void cleanCommand(void *);
void clean_command_part(void *);
void printCommand(FILE *out_file, void * passedIn);

int isBang(const char *command);
void execTheBang(int argc, char **command);
char *getBang(char *command);

#endif //USH_COMMANDS_H
