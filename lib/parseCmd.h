//
// Created by jharshman on 10/15/15.
//

#ifndef USH_PARSE_CMD_H
#define USH_PARSE_CMD_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utility.h"
#include "linkedlist.h"

#define EXECUTE 1
#define REDIRECT 2
#define REDIR_OUT ">"
#define PIPE "|"
LinkedList *parse(char *s);

#endif //USH_PARSE_CMD_H
