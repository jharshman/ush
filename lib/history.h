//
// Created by jharshman on 10/15/15.
//

#ifndef USH_HISTORY_H
#define USH_HISTORY_H
#define HISTFILE ".ush_history"
#define CONFIG ".ushrc"
#define HISTCOUNT_KEY "HISTCOUNT"
#define HISTFILECOUNT_KEY "HISTFILECOUNT"
#define D_HISTFILECOUNT 1000
#define D_HISTCOUNT 100

#include "linkedlist.h"
#include "commands.h"


extern int history_start;
extern int hcount;
extern int hfcount;
extern LinkedList *history;

void addHistory(char *command);
int canAddHistory(char *command);
void flushHistory(FILE *histfile);
void parseHistoryFile(FILE *histfile);
void initHist(int start);
Commands *historyNumbered(int num);

#endif //USH_HISTORY_H
