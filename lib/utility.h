//
// Created by jharshman on 10/9/15.
//

#ifndef LAB5_UTILITY_H
#define LAB5_UTILITY_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 100

char *strip(char *s);
void clean(int argc, char **argv);
char ** makeargs(char *s, int * argc, const char *delim);

#endif //LAB5_UTILITY_H
