# README #


### ush ###

* opsys shell assignment
* Version 0.1

### How to Build ###

    * Install from distro repos `apt-get install make`
    * `make`
    * Binaries will be located in ./bin directory


### Issues ###

* Aliasing feature not working. Feature left out of merge.

### Todo ###

* Fix Aliasing.

### References ###

http://linux.die.net/man/2/ftruncate
http://pubs.opengroup.org/onlinepubs/009695399/functions/fileno.html
http://stephen-brennan.com/2015/01/16/write-a-shell-in-c/
http://man7.org/linux/man-pages/man2/dup.2.html
http://stackoverflow.com/questions/3642732/using-dup2-for-piping
http://stackoverflow.com/questions/5207534/multi-piping-bash-style-in-c
http://stackoverflow.com/questions/1591361/understanding-typedefs-for-function-pointers-in-c-examples-hints-and-tips-ple
http://cboard.cprogramming.com/cplusplus-programming/9089-typedef-functions.html
http://linux.die.net/man/3/execvp

### Who do I talk to? ###

* Josh Harshman (jharshman)
