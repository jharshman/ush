//
// Created by jharshman on 10/15/15.
//

#include "parseCmd.h"
#include "commands.h"


//wrapper for makeargs function
//parses on different i/o redirection types
//specifically stdin and out redir ie: > <, and pipe |
LinkedList *parse(char *s) {
    char *temp_argv[2], **redirectionParts, **pipeParts, **argv;
    int numRedir, numPipe;
    int argc, i;
    LinkedList *returnList = linkedList();

    //tokenize based on stdout redirection
    //REDIR_OUT is equivalent to ">" or redir for stdout
    redirectionParts = makeargs(s, &numRedir, REDIR_OUT);

    //redirection count
    //if equal 2, assign the redirection parts to temp[0] and
    //set temp[1] to nullptr
    if (numRedir == 2) {
        temp_argv[0] = redirectionParts[1];
        temp_argv[1] = NULL;
        //add the node into the list
        addLast(returnList, build_node(build_command_part(1, temp_argv, REDIRECT)));
    }

    //tokenize based on pipe redirection
    //PIPE is "|"
    pipeParts = makeargs(redirectionParts[0], &numPipe, PIPE);

    // if we have pipes, run through them and tokenize
    //this handles multiple pipes
    if (numPipe >= 1) {
        for (i = numPipe - 1; i >= 0; i--) {
            argv = makeargs(pipeParts[i], &argc, " ");
            addFirst(returnList, build_node(build_command_part(argc, argv, EXECUTE)));
            clean(argc, argv);
        }
    }

    if (numRedir != -1)
        clean(numRedir, redirectionParts);

    if (numPipe != -1)
        clean(numPipe, pipeParts);

    return returnList;
}
