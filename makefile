CC=gcc
CFLAGS=-I
MAIN=cscd340_f15_asgn6.c
TARGET=ush
BIN_DIR=bin
OBJ_DIR=obj
LIB_DIR=lib
MKDIR_P=mkdir -p

#define sources
DEPS=utility.h commands.h history.h linkedlist.h parseCmd.h pipe.h
OBJS=cscd340_f15_asgn6.o utility.o commands.o history.o linkedlist.o parseCmd.o pipe.o
OBJS_P=$(patsubst %,$(OBJ_DIR)/%,$(OBJS))

# default target
default: $(TARGET)

# build objects
# drop objects into object directory
%.o: %.c $(MAIN)
	@if [ ! -d "$(OBJ_DIR)" ]; then $(MKDIR_P) $(OBJ_DIR); fi
	@printf "[*] compiling %20s\t" $@
	$(CC) -c -o $(OBJ_DIR)/$@ $< $(CFLAGS) $(LIB_DIR)

# build
$(TARGET):	$(OBJS)
	@if [ ! -d "$(BIN_DIR)" ]; then $(MKDIR_P) $(BIN_DIR); fi
	@printf "[*] linking executable %10s\t" $@
	$(CC) -o $(BIN_DIR)/$@ $(OBJS_P) $(CFLAGS) $(LIB_DIR)
	@if [ -f "$(BIN_DIR)/$(TARGET)" ]; then printf "[+] Build Successful!!\a\n"; fi


# clean
clean:
	@echo "[!] Cleaning"
	$(RM) $(BIN_DIR)/$(TARGET)
	$(RM) $(OBJ_DIR)/*.o
