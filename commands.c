//
// Created by jharshman on 10/15/15.
//

/**
 *
 *
 * implementation of commands.h
 *
 * Provide simple command structure to define
 * a part of a command, and the command parts
 * that make up that command
 * ------------------------------------------------------------
 * void * buildCommand(char *s);
 * void *build_command_part(int argc, char **argv, int type);
 * int commands_are_equal(char *com_a, char *com_b);
 * void cleanCommand(void *);
 * void clean_command_part(void *);
 * void printCommand(FILE *out_file, void * passedIn);
 * int isBang(const char *command);
 * void execTheBang(int argc, char **command);
 * char *getBang(char *command);
 *
 * */

#include "commands.h"
#include "history.h"
#include "parseCmd.h"
#include "pipe.h"

extern int history_start;
static int count = 1;
static executable **shellCommand;

//build function for command
//simple, takes in char * and allocates memory
//it is important to remember that the integer variable num
//is declared with the "static" keyword for a reason,
//the variable is modified by the function each time a command is
//built, thereby incrementing the count.  This will not work if the "static"
//keyword is removed
void * buildCommand(char *s) {
    static int num = 0;
    Commands *command = calloc(1, sizeof(Commands));
    command->command = calloc(strlen(s) + 1, sizeof(char));
    strcpy(command->command, s);
    command->num = history_start + num++;
    return command;
}

//check if the commands are equivalent
int commands_are_equal(char *a, char *b) {
    return strcmp(a, b) == 0;
}

//build part of command
//allocate memory for the command part
//assign argc and type into instantiated structure
void *build_command_part(int argc, char **argv, int type) {
    static int num = 0;
    cmdPart *command = calloc(1, sizeof(cmdPart));
    command->command = calloc((size_t)(argc + 1), sizeof(char*));
    command->count = argc;
    command->type = type;

    size_t i;
    for (i = 0; i < argc; i++) {
        command->command[i] = calloc(strlen(argv[i]) + 1, sizeof(char));
        strcpy(command->command[i], strip(argv[i]));
    }
return command;
}

//print command to file
//used to print the executed command to
//.ush_history file
void printCommand(FILE *out_file, void * passedIn) {
    Commands *command = (Commands*)passedIn;
    fprintf(out_file, "%5d %s\n", command->num, command->command);
}

//clean part
//cleans the allocated memory associated with
//the command part
//generic void ptr to data is passed in, assigned to a
//cmdPart and walked in a for-loop to access and free the
//encapsulated command data
void clean_command_part(void *data) {
    cmdPart *part = (cmdPart*)data;
    for (int i = 0; i < part->count; i++)
        free(part->command[i]);
    free(part->command);
    free(part);
}

//cleans the command
//boring function, works the same as the clean part function
//excluding the for-loop
void cleanCommand(void * command) {
    Commands *com = (Commands*)command;
    free(com->command);
    free(com);
}

//implement simple check
//to see if a command is a bang or not
int isBang(const char *command) {
    return command[0] == BANG;
}

void execTheBang(int argc, char **command) {
    char *bang;
    if (isBang(command[0])) {
        bang = getBang(command[0]);
        if (bang != NULL)
            execCmd(bang);

        free(bang);
        exit(EXIT_SUCCESS);
    }

    int i;
    for (i = 0; i < count; i++)
    if (strcmp(command[0], shellCommand[i]->command) == 0)
        shellCommand[i]->exec(argc, command);

    exit(EXIT_FAILURE);

}

char *getBang(char *command) {
    Commands *cur_command;
    int id, argc = 0;
    char *out = NULL;
    char **argv = NULL;

    if (strcmp(command, BANGBANG) == 0) {
        cur_command = (Commands*)history->head->prev->data;
        argv = makeargs(cur_command->command, &argc, SPACE);
    } else if (sscanf(command, "!%d", &id) == 1) {
        cur_command = historyNumbered(id);
        if (cur_command != NULL)
            argv = makeargs(cur_command->command, &argc, SPACE);
    }

    if (argv != NULL) {
        if (argc > 0) {
            out = calloc(strlen(cur_command->command) + 1, sizeof(char));
            strcpy(out, cur_command->command);
        }
        clean(argc, argv);
    }

    return out;
}

