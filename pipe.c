//
// Created by jharshman on 10/15/15.
//

#include "pipe.h"
#include <unistd.h>
#include <sys/wait.h>
#include "linkedlist.h"
#include "parseCmd.h"
#include "commands.h"

void execCmd(char *command) {

    int *pipes;
    int status;
    int i;
    int child_num;
    pid_t *child_pids;
    pid_t child_pid;
    LinkedList *commands;
    FILE *redirect = NULL;
    Node *cur;

    cmdPart *current_command, *next_command = NULL;

    commands = parse(command);

    pipes = calloc(commands->size * 2, sizeof(int));
    child_pids = calloc(commands->size * 2, sizeof(pid_t));

    for (i = 0; i < commands->size; i++)
        if (pipe(pipes + (i * 2)) < 0)
            exit(EXIT_FAILURE);

    cur = commands->head;

    child_num = 0;

    while ((cur = cur->next) != commands->head) {
        current_command = (cmdPart *) cur->data;

        if (current_command->type == REDIRECT)
            continue;

        if (cur->next != commands->head) {
            next_command = (cmdPart *) cur->next->data;

            if (next_command->type == REDIRECT) {
                redirect = fopen(next_command->command[0], "w+");
                close(pipes[child_num * 2 + 1]);
                pipes[child_num * 2 + 1] = fileno(redirect);
            }
        }

        child_pid = fork();
        child_pids[child_num] = child_pid;

        if (child_pid == -1) {
            exit(EXIT_FAILURE);
        } else if (child_pid == 0) {
            if (child_num > 0) {
                if (dup2(pipes[(child_num - 1) * 2], STDIN_FILENO) < 0) {
                    exit(EXIT_FAILURE);
                }
            }

            if (cur->next != commands->head) {
                if (dup2(pipes[(child_num * 2) + 1], STDOUT_FILENO) < 0) {
                    exit(EXIT_FAILURE);
                }
            }

            for (i = 0; i < commands->size * 2; i++)
                close(pipes[i]);

            if (isBang(current_command->command[0])) {
                execTheBang(current_command->count, current_command->command);
            } else {
                execvp(current_command->command[0], current_command->command);
            }

            exit(EXIT_FAILURE);
        }

        if (redirect != NULL) {
            fclose(redirect);
            redirect = NULL;
        }

        next_command = NULL;
        child_num++;
    }

    for (i = 0; i < commands->size * 2; i++)
        close(pipes[i]);

    for (i = 0, cur = commands->head->next; i < commands->size; i++, cur = cur->next) {
        current_command = (cmdPart *) cur->data;

        waitpid(child_pids[i], &status, 0);

        if (WIFEXITED(status) && WEXITSTATUS(status) == EXIT_FAILURE) {
            if (!isBang(current_command->command[0]))
                fprintf(stderr, "Invalid command %s\n", current_command->command[0]);
        }
    }

    fflush(stdout);
    clearList(commands, clean_command_part);
    free(commands);
    free(pipes);
    free(child_pids);
}

